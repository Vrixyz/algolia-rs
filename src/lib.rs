extern crate reqwest;
extern crate serde;
extern crate serde_json;

#[macro_use]
extern crate serde_derive;

use std::collections::HashMap;

#[derive(Deserialize, Debug)]
pub struct HighlightResult {
    pub value: Option<String>,
    pub highlighted: Option<String>,
    /// FIXME: Should be an enum
    pub matchLevel: String,
}

#[derive(Deserialize, Debug)]
pub struct RankingInfo {
    pub nbTypos: i32,
    pub firstMatchedWord: i32,
    pub proximityDistance: i32,
    pub userScore: i32,
    pub geoDistance: i32,
    pub geoPrecision: i32,
    pub nbExactWords: i32,
}

#[derive(Deserialize, Debug)]
pub struct QueryHit<T> {
    /// FIXME: Should be dynamic, depending on schema
    #[serde(flatten)]
    pub record: T,
    pub objectID: String,
    pub _highlightResult: HashMap<String, HighlightResult>,
    pub _snippetResult: Option<serde_json::Value>,
    pub _rankingInfo: Option<RankingInfo>,
}

#[derive(Deserialize, Debug)]
pub struct QueryResponse<T> {
    pub hits: Vec<QueryHit<T>>,
    pub page: i32,
    pub nbHits: i32,
    pub hitsPerPage: i32,
    pub processingTimeMS: i32,
    pub query: String,
    pub parsed_query: Option<String>,
    pub params: String,
}

#[derive(Debug)]
pub struct Error;

impl From<reqwest::Error> for Error {
    fn from(err: reqwest::Error) -> Error {
        println!("{:#?}", err);
        Error {}
    }
}

const primary_host: &str = "https://H8SWW455HV-dsn.algolia.net";

/// # Arguments
///
/// * `string` - a string expected to be valid json
pub fn search<T>(string: &str) -> Result<QueryResponse<T>, Error>
where
    for<'de> T: serde::Deserialize<'de>,
{
    let query = format!("{{\"params\": \"query={}&hitsPerPage={}&getRankingInfo={}\"}}", string, 2, 1);
    let mut response = reqwest::Client::new()
        .post(&(String::from(primary_host) + "/1/indexes/getstarted_actors/query"))
        .header("X-Algolia-Application-Id", "H8SWW455HV")
        .header("X-Algolia-API-Key", "c804bca5e60f0a41e7617740350f6abb")
        // TODO: escape string as html
        .body(String::from(query))
        .send()?;

    //println!("{:#?}", response.text());

    let result = response.json()?;
    // TODO: implement retry strategy
    return Result::Ok(result);
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        #[derive(Deserialize, Debug)]
        pub struct CustomRecord {
            pub name: String,
            pub rating: i32,
            pub alternative_name: Option<String>,
            pub image_path: String,
        };

        let response = ::search::<CustomRecord>("test");
        assert!(response.is_ok());
        println!("search_result: {:#?}", response);
    }
}
